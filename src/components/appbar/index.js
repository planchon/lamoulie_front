import React from "react"

import { AppBar, Toolbar } from "@material-ui/core"

import logo from "./logo_text.png"

export default class SiteAppbar extends React.Component {
    render() {
        return (
            <AppBar color="inherit">
                <Toolbar>
                    <img src={logo} alt="logo" style={{height: 35}}/>
                </Toolbar>
            </AppBar>
        )
    }
}