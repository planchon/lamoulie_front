import logo from './logo.svg';
import './App.css';
import IndexPage from './pages';

function App() {
  return (
    <IndexPage></IndexPage>
  );
}

export default App;
