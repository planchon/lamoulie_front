const IS_PROD = false

export const BACK_URL = (IS_PROD) ? "http://lamoulie.casa-de-paupel.fr:8080" : "http://localhost:8080"