import { Container, IconButton, Button, Paper, Slider, Table, TableRow, Snackbar, SnackbarContent, TableHead, TableBody, TableCell, TableContainer, Typography, Grid, TextField } from "@material-ui/core"
import React from "react"
import GoogleLogin from 'react-google-login';
import logo from "./logo.png"

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import {BACK_URL} from "../constants"

import MuiAlert from '@material-ui/lab/Alert';

import SiteAppbar from "../components/appbar"

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default class IndexPage extends React.Component {

    constructor(props) {
        super(props)

        this.googleCallback = this.googleCallback.bind(this)
        this.sendLamoulie = this.sendLamoulie.bind(this)
        this.close_snackbar = this.close_snackbar.bind(this)

        this.state = {
            lamoula_loaded: false,
            lamoula_data: [],
            google: false,
            google_profile: [],
            google_id: "",
            user_lamoulie: "",
            snack: {
                open: false,
                data: "",
                severity: ""
            },
            user_loaded: false
        }
    }

    googleCallback(e) {
        if (e.googleId) {
            if (e.profileObj.email.split("@")[1] == "eisti.eu") {
                this.setState({google: true, google_profile: e.profileObj, google_id: e.tokenId})
                this.fetchUserData()
            } else {
                let snack_data = {open: true, data: "Il faut s'enregistrer avec une adresse mail eisti.eu", severity: "error"}
                this.setState({snack: snack_data})
            }
        }
    }

    fetchAllData() {
        if (this.state.user_loaded) {
            this.fetchUserData()
        }
        this.fetchData()
    }

    componentDidMount() {
        this.fetchData()
    }

    fetchData() {
        this.setState({lamoula_loaded: false})
        fetch(BACK_URL + "/lamoulie", { method: "GET"}).then(js => js.json())
        .then(data => {
            this.setState({lamoula_loaded: true, lamoula_data: data})
        })
    }

    fetchUserData() {
        this.setState({user_loaded: false})
        fetch(BACK_URL + "/user", { 
            method: "POST", 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            }, 
            body: JSON.stringify({
                id: this.state.google_id,
                gprof: this.state.google_profile,
            })
        }).then(js => js.json())
        .then(data => {
            console.log("user", data)
            this.setState({user_loaded: true, user_data: data})
        })
    }

    colorButton(id, type) {
        if (this.state.user_loaded) {
            let vote = this.state.user_data.filter(e => e.vote_id == id)
            if (vote.length > 0) {
                return (vote[0].vote == type) ? "primary" : ""
            } else {
                return ""
            }
        } else {
            return ''
        }
    }

    renderTable() {
        if (this.state.lamoula_data.length == 0) {
            return <center>pas de lamoulie d'or pour l'instant 🤭</center>
        }
        return (
            <Grid container spacing={3}>
                {this.state.lamoula_data.map(e => {
                    return (
                        <Grid item xs={12} md={4}>
                            <Card variant="outlined">
                                <CardContent>
                                    <Typography color="textSecondary" gutterBottom>
                                        up : {(e.votes_p / (e.votes_n + e.votes_p) * 100).toFixed(0)}%, {(e.votes_n + e.votes_p)} votes
                                    </Typography>   
                                    <Typography variant="h6">
                                        {e.title}
                                    </Typography>
                                </CardContent>
                                <CardActions disableSpacing>
                                    <IconButton onClick={() => {
                                        this.sendVote(e['id'], 1)
                                    }} color={this.colorButton(e["id"], 1)}>
                                        <ArrowUpwardIcon />
                                    </IconButton>
                                    <IconButton onClick={() => {
                                        this.sendVote(e['id'], -1)
                                    }} color={this.colorButton(e["id"], -1)}>
                                        <ArrowDownwardIcon />
                                    </IconButton>
                                </CardActions>
                            </Card>
                        </Grid>
                    )
                })}
            </Grid>
        )
    }

    sendVote(vote_id, vote_type) {
        fetch(BACK_URL + "/vote", { 
            method: "POST", 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            }, 
            body: JSON.stringify({
                id: this.state.google_id,
                gprof: this.state.google_profile,
                vote_id: vote_id,
                vote_type: vote_type
            })
        }).then(data => data.json())
        .then(e => {
            if (e.status == "vote_ok") {
                let snack_data = {open: true, data: "Vote enregistré 😘", severity: "success"}
                // this.state.user_loaded.push(e["vote"])
                this.fetchUserData()
            }
            if (e.status == "non_log") {
                let snack_data = {open: true, data: "Enregistre toi avec Google pour voter", severity: "error"}
                this.setState({snack: snack_data})
                this.fetchAllData()
            }
        })
    }

    sendLamoulie() {
        fetch(BACK_URL + "/", { 
            method: "POST", 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            }, 
            body: JSON.stringify({
                id: this.state.google_id,
                gprof: this.state.google_profile,
                lamoulie: this.state.user_lamoulie
            })
        }).then(data => data.json()).then(e => {
            if (e.status == "ok") {
                let snack_data = {open: true, data: "Lamoulie d'or enregistré ! Merci 😘", severity: "success"}
                this.setState({snack: snack_data, user_lamoulie: ""})
                this.fetchAllData()
            } 
            if (e.status == "too_many") {
                let snack_data = {open: true, data: "Tu as envoyé trop d'idée de Lamoulie d'or, merci 🤓 !", severity: "warning"}
                this.setState({snack: snack_data})
                console.log("too many lamoulie")
            }
        })
    }

    ajoutLamoulie() {
        return (
            <div>
                <Typography variant="h6" style={{paddingBottom: 10}}>Propose ton lamoulie d'or, {this.state.google_profile.givenName} !</Typography>
                <TextField variant="outlined" style={{width: "100%", marginBottom: 10}} label={"Super idée de Lamoulie d'or"} value={this.state.user_lamoulie} onChange={(e) => {this.setState({user_lamoulie: e.target.value})}}></TextField>
                <center>
                    <Button color="secondary" variant="outlined" style={{marginBottom: 10}} onClick={() => this.sendLamoulie()}>Envoyer</Button>
                </center>
            </div>
        )
    }

    googleButton() {
        return (
            <center style={{marginBottom: 20}}>
                <GoogleLogin
                    clientId="830020371523-piafssrhj1li0rghh9tevh55j07nrk62.apps.googleusercontent.com"
                    buttonText="Se connecter"
                    onSuccess={this.googleCallback}
                    onFailure={this.googleCallback}
                    cookiePolicy={'single_host_origin'}
                />
            </center>
        )
    }

    close_snackbar() {
        let snack_data = {open: false, data: "", severity: ""}
        this.setState({snack: snack_data})
    }

    snackbar() {
        return (
            <Snackbar open={this.state.snack.open} autoHideDuration={6000} onClose={() => {this.close_snackbar()}}>
                <Alert onClose={() => {this.close_snackbar()}} severity={this.state.snack.severity}>
                    {this.state.snack.data}
                </Alert>
            </Snackbar>
        )
    }

    render() {
        return (
            <div>
                <SiteAppbar></SiteAppbar>
                <Container style={{paddingTop: 70}}>
                    <center style={{padding: 10}}>
                        <img src={logo} style={{height: "25vw"}}/>
                    </center>
                    <Alert severity="info" style={{marginBottom: 20}}>Vous devez vous connecter avec votre adresse mail <strong>eisti.eu</strong> pour intéragir</Alert>
                    <Alert severity="warning" style={{marginBottom: 20}}>On ne vous demande pas d'élir une personne pour un Lamoulie d'or (pour l'instant), mais de choisir quels Lamoulie d'or nous allons decerner cette année</Alert>
                    <SnackbarContent style={{marginBottom: 20}} message={"Vérifiez bien que votre idée n'a pas déjà été proposée avant de l'ajouter, vous pouvez ajouter 5 lamoulie d'or au maximum!"} />
                    {this.state.google ? this.ajoutLamoulie() : this.googleButton()}
                    {this.state.lamoula_loaded ? this.renderTable(): <div>loading</div> }
                    {this.snackbar()}
                </Container>
            </div>
        )
    }
}